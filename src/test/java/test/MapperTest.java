package test;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.kami.dao.SysUserMapper;
import com.kami.entity.SysUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class MapperTest {
	
	
	@Autowired
	private SysUserMapper sysUserMapper;
	
	@Test
	public void testCRUD(){
		System.out.println(sysUserMapper);
		sysUserMapper.insertSelective(new SysUser(null,"jwz", "jwz", "123456", new Date()));
	}

}
