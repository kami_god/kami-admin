function showTipMsg(msg){
	$.messager.show({
		title:'系统提示',
		msg:msg,
		showType:'show'
	});
}

function showTopTipMsg(msg){
	$.messager.show({
		title:'系统提示',
		msg:msg,
		showType:'show',
		style:{
			right:'',
			top:document.body.scrollTop+document.documentElement.scrollTop+10,
			bottom:''
		}
	});
}

window.confirm = function(msg,callback){
	$.messager.confirm('系统提示', msg, function(r){
		if (r){
			if(typeof(callback) === "function"){
				callback(r);
			}
		}
	});

}