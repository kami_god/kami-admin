<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="edge" />

<!-- JQuery -->
<script type="text/javascript" src="${base }/static/js/jquery.min.js"></script>

<!-- EasyUI 1.4.3 -->
<link rel="stylesheet" type="text/css" href="${base }/static/easyui/themes/gray/easyui.css">
<link rel="stylesheet" href="${base }/static/easyui/themes/icon.css">
<script type="text/javascript" src="${base }/static/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${base }/static/easyui/locale/easyui-lang-zh_CN.js"></script>

<!-- vue.js -->
<script type="text/javascript" src="${base }/static/js/vue.min.js"></script>

<!-- ztree -->
<link rel="stylesheet" type="text/css" href="${base }/static/ztree/css/zTreeStyle.css" />
<script type="text/javascript" src="${base }/static/ztree/js/jquery.ztree.core.js" charset="utf-8"></script>
<script type="text/javascript" src="${base }/static/ztree/js/jquery.ztree.excheck.js" ></script>

<!-- 图标字体 -->
<link rel="stylesheet" type="text/css" href="${base }/static/foundation-icons/foundation-icons.css" />

<!-- 自定义common.js -->
<script type="text/javascript" src="${base }/static/js/common.js"></script>
<link rel="stylesheet" type="text/css" href="${base }/static/css/icon-show.css" />

<!-- 自定义扩展extJs -->
<script type="text/javascript" src="${base }/static/js/extJs.js"></script>
