<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="/commons/basejs.jsp" %>
    <title>主页</title>
    <style>
		#head_logo{
			height:60px;
			margin-top:10px;
		}
		  /*ie6提示*/
        #ie6-warning{width:100%;position:absolute;top:0;left:0;background:#fae692;padding:5px 0;font-size:12px}
        #ie6-warning p{width:960px;margin:0 auto;}
    </style>
    <script type="text/javascript">
		    var index_tabs;
		    var indexTabsMenu;
		    var indexMenuZTree;
			$(function(){
				 $('#index_layout').layout({fit : true});
				 indexMenuZTree = $.fn.zTree.init($("#treeDemo"), setting);
				 
				index_tabs = $('#index_tabs').tabs({
		            fit : true,
		            border : false,
		            onContextMenu : function(e, title) {
		                e.preventDefault();
		                indexTabsMenu.menu('show', {
		                    left : e.pageX,
		                    top : e.pageY
		                }).data('tabTitle', title);
		            },
		            tools : [{
		                iconCls : 'fi-home',
		                handler : function() {
		                    index_tabs.tabs('select', 0);
		                }
		            }, {
		                iconCls : 'fi-loop',
		                handler : function() {
		                    refreshTab();
		                }
		            }, {
		                iconCls : 'fi-x',
		                handler : function() {
		                    var index = index_tabs.tabs('getTabIndex', index_tabs.tabs('getSelected'));
		                    var tab = index_tabs.tabs('getTab', index);
		                    if (tab.panel('options').closable) {
		                        index_tabs.tabs('close', index);
		                    }
		                }
		            } ]
		        });
		        
		        
		  // 选项卡菜单    右键点击tab关闭所有
		indexTabsMenu = $('#tabsMenu').menu({
		    onClick : function(item) {
		        var curTabTitle = $(this).data('tabTitle');
		        var type = $(item.target).attr('type');
		        if (type === 'refresh') {
		            refreshTab();
		            return;
		        }
		        if (type === 'close') {
		            var t = index_tabs.tabs('getTab', curTabTitle);
		            if (t.panel('options').closable) {
		                index_tabs.tabs('close', curTabTitle);
		            }
		            return;
		        }
		        var allTabs = index_tabs.tabs('tabs');
		        var closeTabsTitle = [];
		        $.each(allTabs, function() {
		            var opt = $(this).panel('options');
		            if (opt.closable && opt.title != curTabTitle
		                    && type === 'closeOther') {
		                closeTabsTitle.push(opt.title);
		            } else if (opt.closable && type === 'closeAll') {
		                closeTabsTitle.push(opt.title);
		            }
		        });
		        for ( var i = 0; i < closeTabsTitle.length; i++) {
		            index_tabs.tabs('close', closeTabsTitle[i]);
		        }
		    }
		});
			});
			
			    function addTab(opts) {
			        var t = $('#index_tabs');
			        if (t.tabs('exists', opts.title)) {
			            t.tabs('select', opts.title);
			        } else {
			            t.tabs('add', opts);
			        }
			    }
			
			    function refreshTab() {
			        var index = index_tabs.tabs('getTabIndex', index_tabs.tabs('getSelected'));
			        var tab = index_tabs.tabs('getTab', index);
			        var options = tab.panel('options');
			        if (options.content) {
			            index_tabs.tabs('update', {
			                tab: tab,
			                options: {
			                    content: options.content
			                }
			            });
			        } else {
			            tab.panel('refresh', options.href);
			        }
			    }
			    
		
			
		var setting = {
			data: {
				key: {
					name:"text"
				},
				simpleData: {
					enable: true,
					idKey: "id",
	                pIdKey: "pId",
	                rootPId: 1
				}
			},
            async: {
                enable: true,
                url:"${base}/sys/resource/tree",
                dataFilter: function (treeId, parentNode, responseData) {
                    if (responseData) {
                        for (var i =0; i < responseData.length; i++) {
                            var node = responseData[i];
                            if (node.state == "open") {
                                node.open = true;
                            }
                        }
                    }
                    return responseData;
                }
            },
			callback: {
				onClick: onClick
			}
		};
		function onClick(event, treeId, treeNode, clickFlag) {
	 		var node = treeNode;
		    var opts = {
		        title : node.text,
		        border : false,
		        closable : true,
		        fit : true,
		        iconCls : node.iconCls
		    };
		    var url = node.attributes;
            if (url && url.indexOf("http") == -1) {
                url = '${base }/' + url;
            }
		    if (node.openMode == "iframe") {
		    	// iframe 到达的页面需要<html>标签才能显示
		        opts.content = '<iframe src="' + url + '" frameborder="0" style="border:0;width:100%;height:99.0%;"></iframe>';
		        addTab(opts);
		    } else if (url) {
		    	// ajax  到达的页面不需要 <html>等标签（有html标签就显示不出来）
		    	 opts.href = url;
                 addTab(opts);
		    } 
		}		

    </script>
</head>
<body >
	    <div id="loading" style="position: fixed;top: -50%;left: -50%;width: 200%;height: 200%;background: #fff;z-index: 100;overflow: hidden;">
	        <img src="${base }/static/images/ajax-loader.gif" style="position: absolute;top: 0;left: 0;right: 0;bottom: 0;margin: auto;"/>
	    </div>
		<div id="index_layout">
		        <div data-options="region:'north',border:false" style="overflow: hidden;">
		        	<div  style="height:80px;">
		        		<span>
		        			<img  id="head_logo" alt="" src="${base }/static/icon/logo.png">
		        		</span>
			        	 <span style="float: right; padding-right: 20px; margin-top: 15px; color: #333">
		                    <i class="fi-torso"></i>
		                    <b>欢迎 :<shiro:principal property="loginname"></shiro:principal></b> 
		                    <a href="logout"  class="easyui-linkbutton" plain="true" icon="fi-x">安全退出</a>
		                </span>
		        	</div>
    			</div>
    			
    			 <div data-options="region:'west',split:true" title="菜单" style="width: 160px; overflow: hidden;overflow-y:auto; padding:0px">
        			<ul id="treeDemo" class="ztree"></ul>
    			 </div>
    			
    			 <div data-options="region:'center'" style="overflow: hidden;">
    			 	 <div id="index_tabs" style="overflow: hidden;">
	                          <div title="首页" data-options="iconCls:'fi-home',border:false" style="overflow: hidden;">
			                    <script src='http://git.oschina.net/kami_god/kami-admin/widget_preview'></script>
			                    <style>
			                        .pro_name a{color: #4183c4;}
			                        .osc_git_title{background-color: #d8e5f1;}
			                        .osc_git_box{background-color: #fafafa;}
			                        .osc_git_box{border-color: #ddd;}
			                        .osc_git_info{color: #666;}
			                        .osc_git_main a{color: #4183c4;}
			                    </style>
			                </div>
           			 </div>
    			</div>
    			
    			<div data-options="region:'south',border:false" style="height: 30px;line-height:30px; overflow: hidden;text-align: center;background-color: #eee" >Copyright © 2017 power by <a href="#" target="_blank">kami-admin</a></div>
		</div>
		
		<div id="tabsMenu">
	        <div data-options="iconCls:'fi-loop'" type="refresh" style="font-size: 12px;">刷新</div>
	        <div class="menu-sep"></div>
	        <div data-options="iconCls:'fi-x'" type="close" style="font-size: 12px;">关闭</div>
	        <div data-options="iconCls:''" type="closeOther">关闭其他</div>
	        <div data-options="iconCls:''" type="closeAll">关闭所有</div>
	    </div>
	    
	    
	      <!--[if lte IE 7]>
    <div id="ie6-warning"><p>您正在使用 低版本浏览器，在本页面可能会导致部分功能无法使用。建议您升级到 <a href="http://www.microsoft.com/china/windows/internet-explorer/" target="_blank">Internet Explorer 8</a> 或以下浏览器：
    <a href="http://www.mozillaonline.com/" target="_blank">Firefox</a> / <a href="http://www.google.com/chrome/?hl=zh-CN" target="_blank">Chrome</a> / <a href="http://www.apple.com.cn/safari/" target="_blank">Safari</a> / <a href="http://www.operachina.com/" target="_blank">Opera</a></p></div>
    <![endif]-->
    
</body>
</html>