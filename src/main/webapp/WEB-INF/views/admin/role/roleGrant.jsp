<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<script>
var grantZtree;
var roleId = '${id}';
$(function(){
	getMenuTree(roleId);
	
	
	
});

var setting_role = {
		check: {
            enable:true,
		},
		data: {
			key: {
				name:"text"
			},
			simpleData: {
				enable: true,
				idKey: "id",
                pIdKey: "pId",
                rootPId: null
			}
		}
	};

$("#roleGrantForm").form({
	url:'${base}/sys/role/grant',
 	onSubmit: function(){    
 		// do some check
 		var nodes = grantZtree.getCheckedNodes(true);
 		var menuIdList = [];
 		for(var i=0;i<nodes.length;i++){
 			menuIdList.push(nodes[i].id);
 		}
 		$("#menuIdList").val(menuIdList);
 	     progressLoad();
         var isValid = $(this).form('validate');
         if (!isValid) {
             progressClose();
         }
         return isValid;
    },    
    success:function(res){ 
    	progressClose();
    	res = $.parseJSON(res);
    	if(res.success){
    		$.modalDialog.handler.dialog('close');// 父页面dialog close
    		$.messager.alert('系统消息',res.msg,'info');
    	}
    }    

});
function getMenuTree(roleId){
	$.ajax({
		type:'POST',
		url:'${base}/sys/resource/allTree',
		async:true,
		dataType:'json',
		success:function(res){
			grantZtree = $.fn.zTree.init($("#grantTree"), setting_role,res);
			grantZtree.expandAll(true);
		},
   		complete:function(){
	        if(roleId != null){
	             getRoleList(roleId);
	        }
     	}
	});
}

function getRoleList(roleId){
	$.post("${base}/sys/role/getMenuIdListByRoleId",{id:roleId},function(res){
		var menuIds = res.menuIdList;
		for(var i=0;i<menuIds.length;i++){
			var node = grantZtree.getNodeByParam("id", menuIds[i]);
			grantZtree.checkNode(node,true,false);
		}
		
	});
}

function selectAll(){
	grantZtree.checkAllNodes(true);
}
function cancelSelectAll(){
	grantZtree.checkAllNodes(false);
}
function reverseSelect(){
	var nodes = grantZtree.transformToArray(grantZtree.getNodes());// 递归获取所有节点
	for(var i=0;i<nodes.length;i++){
		grantZtree.checkNode(nodes[i],null,false);
	}
}
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
   <div data-options="region:'west',border:false,split:true" title="系统资源" style="overflow: hidden;padding: 2px;width:380px;">
 		<form id="roleGrantForm" method="post" >
			<ul id="grantTree" class="ztree"></ul>
			<input type="hidden" name="menuIdList" id="menuIdList" >
			<input type="hidden" name="roleId"  value="${id}">
 		</form>
   </div>
    <div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 10px;">
    	<div style="margin-bottom:20px">
			<a href="#" onclick="selectAll();" class="easyui-linkbutton" data-options="iconCls:'fi-check'" style="width:80px">全选</a>
		</div>
    	<div style="margin-bottom:20px">
			<a href="#" onclick="reverseSelect();" class="easyui-linkbutton" data-options="iconCls:'fi-refresh'" style="width:80px">反选</a>
		</div>
    	<div style="margin-bottom:20px">
			<a href="#" onclick="cancelSelectAll();" class="easyui-linkbutton" data-options="iconCls:'fi-x'" style="width:80px">全部取消</a>
		</div>
    	
    </div>
</div>