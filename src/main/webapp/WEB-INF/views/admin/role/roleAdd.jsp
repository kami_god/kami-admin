<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<script>
$("#roleAddForm").form({
	url:'${base}/sys/role/add',
 	onSubmit: function(){    
 		// do some check
 	     progressLoad();
         var isValid = $(this).form('validate');
         if (!isValid) {
             progressClose();
         }
         return isValid;
    },    
    success:function(res){ 
    	progressClose();
    	res = $.parseJSON(res);
    	if(res.success){
    		roleDataGrid.datagrid('reload'); // 父页面datagrid reload
    		$.modalDialog.handler.dialog('close');// 父页面dialog close
    	}
    }    

});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 30px;">
        <form id="roleAddForm" method="post">
             <table class="grid">
                <tr>
                    <td>角色名称</td>
                    <td colspan="2"><input name="roleName" type="text" placeholder=" 角色名称" class="easyui-validatebox textbox" style="width:220px;height:30px;" data-options="required:true" value=""></td>
                </tr>
                <tr>
                    <td>备注</td>
                    <td colspan="2"><input name="remark" type="text" data-options="prompt:' 备注'" class="easyui-textbox"  style="width:220px;height:30px;" value=""></td>
                </tr>
            </table>
        </form>
    </div>
</div>