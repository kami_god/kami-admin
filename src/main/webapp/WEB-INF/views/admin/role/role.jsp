<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
    <script type="text/javascript">
    var roleDataGrid;
    $(function(){
    	roleDataGrid = $("#roleDataGrid").datagrid({
    		 url:'${base}/sys/role/dataGrid',    
 		     columns:[[
				{
				    field : 'ck',
				    checkbox : true
				},
 		        {field:'roleId',title:'角色id',width:100,sortable:true,align:'center'},    
 		        {field:'roleName',title:'角色名',width:100,align:'center'},    
 		        {field:'remark',title:'备注',width:300,align:'center'},    
 		        {field:'createTime',title:'创建时间',width:200,align:'center'},
 		        {field:'operation',title:'操作',align:'center',width:200,
		        	formatter : function(value, row, index) {
		        	   var str = '';
		        	   str+=$.formatString('<a href="javascript:void(0)" class="role-easyui-linkbutton-grant" data-options="plain:true,iconCls:\'fi-check icon-green\'" onclick="grant(\'{0}\');" >授权</a>',row.roleId);
		        	   str+=$.formatString('| <a href="javascript:void(0)" class="role-easyui-linkbutton-edit" data-options="plain:true,iconCls:\'fi-pencil icon-blue\'" onclick="editRole(\'{0}\');" >编辑</a>',row.roleId);
		        	   str+=$.formatString('| <a href="javascript:void(0)" class="role-easyui-linkbutton-delete" data-options="plain:true,iconCls:\'fi-x icon-red\'" onclick="deleteRole(\'{0}\');" >删除</a>',row.roleId);
		        	   return str;
		        }},
 		       
 		    ]],
 		    fit : true,
 		    fitColumns:false,//自动宽度
 		    singleSelect:true,//单选
 	        pagination: true,//分页
             rownumbers: true,//行数
             pageSize: 20,//默认选择的分页是每页20行数据
             pageList: [10, 20, 30, 50, 100],//可以选择的分页集合
             nowrap: true,//设置为true，当数据长度超出列宽时将会自动截取
             striped: true,//设置为true将交替显示行背景。
             toolbar : '#roleToolbar',
             onLoadSuccess:function(data){
                 $('.role-easyui-linkbutton-grant').linkbutton({text:'授权'});
                 $('.role-easyui-linkbutton-edit').linkbutton({text:'编辑'});
                 $('.role-easyui-linkbutton-delete').linkbutton({text:'删除'});
             }
    	});
    	
    });
    
    
    function addRole(){
    	$.modalDialog({    
    	    title: '添加',    
    	    width: 500,    
    	    height: 250,    
    	    href: '${base}/sys/role/addRolePage',    
    	    modal: true,
            buttons : [ {
                text : '添加',
                handler : function() {
                	var f = $.modalDialog.handler.find('#roleAddForm');
                	f.submit();
                }
            } ]
    	}); 
    }
    
    function deleteRole(id){
     	 $.messager.confirm('提示', '确认要删除吗？', function(r){
			if (r){
				$.post('${base}/sys/role/delete',{
					id:id
				},function(res){
					if(res.success){
						$.messager.alert('系统消息',res.msg,'info');
						roleDataGrid.datagrid('reload');
					}else{
						$.messager.alert('系统消息','删除失败！','info');
					}	
				},'JSON');
			}
		});
    }
    
    function editRole(id){
       	$.modalDialog({    
    	    title: '编辑',    
    	    width: 500,    
    	    height: 250,    
    	    href: '${base}/sys/role/editRolePage?id='+id,    
    	    modal: true,
            buttons : [ {
                text : '修改',
                handler : function() {
                	var f = $.modalDialog.handler.find('#roleEditForm');
                	f.submit();
                }
            } ]
    	});  
    }
    
    function grant(id){
       	$.modalDialog({    
    	    title: '授权',    
    	    width: 500,    
    	    height: 480,    
    	    href: '${base}/sys/role/grantPage?id='+id,    
    	    modal: true,
            buttons : [ {
                text : '保存',
                handler : function() {
                	var f = $.modalDialog.handler.find('#roleGrantForm');
                	f.submit();
                }
            } ,{
                text : '取消',
                handler : function() {
                	$.modalDialog.handler.dialog('close');
                }
            } ]
    	});
    }
    
</script>

<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'center',border:false"  style="overflow: hidden;">
        <table id="roleDataGrid"></table>
    </div>
</div>
<div id="roleToolbar" style="display: none;">
        <a onclick="addRole();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-plus icon-green'">添加</a>
</div>

