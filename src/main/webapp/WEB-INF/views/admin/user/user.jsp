<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
    <script type="text/javascript">
    	var userDataGrid;
    	$(function(){
    		userDataGrid = $('#userDataGrid').datagrid({    
    		    url:'${base}/sys/user/dataGrid',    
    		    columns:[[
					{
					    field : 'ck',
					    checkbox : true
					},
    		        {field:'id',title:'用户ID',width:100,sortable:true,align:'center'},    
    		        {field:'loginname',title:'登录名',width:100,align:'center'},    
    		        {field:'username',title:'用户名',width:100,align:'center'},    
    		        {field:'email',title:'邮箱',width:150,align:'center'},    
    		        {field:'disable',title:'状态',width:50,align:'center',
    		        	formatter : function(value, row, index) {
    		        	value = parseInt(value);
                        switch (value) {
                        case 0:
                            return '<font class="icon-green">正常<font/>';
                        case 1:
                            return '<font class="icon-red">停用<font/>';
                        }
                    }},    
    		        {field:'phone',title:'电话号码',width:100,align:'center'},    
    		        {field:'createDate',title:'创建时间',width:200,sortable:true,align:'center'},
    		        {field:'operation',title:'操作',align:'center',width:120,
    		        	formatter : function(value, row, index) {
    		        	   var str = '';
    		        	   str+=$.formatString('<a href="javascript:void(0)" class="user-easyui-linkbutton-edit" data-options="plain:true,iconCls:\'fi-pencil icon-blue\'" onclick="editUser(\'{0}\');" >编辑</a>',row.id);
    		        	   return str;
    		        }},
    		    ]],
    		    fit : true,
    		    fitColumns:false,//自动宽度
    	        pagination: true,//分页
                rownumbers: true,//行数
                pageSize: 20,//默认选择的分页是每页20行数据
                pageList: [10, 20, 30, 50, 100],//可以选择的分页集合
                nowrap: true,//设置为true，当数据长度超出列宽时将会自动截取
                striped: true,//设置为true将交替显示行背景。
                toolbar : '#userToolbar',
                onLoadSuccess:function(data){
                    $('.user-easyui-linkbutton-edit').linkbutton({text:'编辑'});
                }
    		});  
    	});
    	
        function searchUser() {
            userDataGrid.datagrid('load', $.serializeObject($('#searchUserForm')));
        }
        
        function cleanUser(){
        	$("#searchUserForm input").val('');
        	userDataGrid.datagrid('load', {});
        }
        
        function addUser(){
        	$.modalDialog({    
        	    title: '添加',    
        	    width: 500,    
        	    height: 350,    
        	    href: '${base}/sys/user/addPage',    
        	    modal: true,
                buttons : [ {
                    text : '添加',
                    handler : function() {
                    	var f = $.modalDialog.handler.find('#userAddForm');
                    	f.submit();
                    }
                } ]
        	});  
        }
        
        function deleteUser(){
        	 var rows = userDataGrid.datagrid('getSelections');
        	 if(rows.length == 0){
        		 return;
        	 }
        	 $.messager.confirm('提示', '确认要删除吗？', function(r){
        			if (r){
        			    var names = [];
                        for (var i = 0; i < rows.length; i++) {
                            names.push(rows[i].id);
                        }
        				$.post('${base}/sys/user/delete',{
        					ids:names
       					},function(res){
       						if(res.success){
       							$.messager.alert('系统消息',res.msg,'info');
       							userDataGrid.datagrid('reload');
       						}else{
       							$.messager.alert('系统消息','删除失败！','info');
       						}	
     					},'JSON');
        			}
        		});
        }
        
        function editUser(id){
        	$.modalDialog({    
        	    title: '编辑',    
        	    width: 500,    
        	    height: 350,    
        	    href: '${base}/sys/user/editUserPage?id='+id,    
        	    modal: true,
                buttons : [ {
                    text : '修改',
                    handler : function() {
                    	var f = $.modalDialog.handler.find('#userEditForm');
                    	f.submit();
                    }
                } ]
        	});  
        }
        
        $(document).keyup(function(event){
        	  if(event.keyCode ==13){
        		  searchUser();
        	  }
        	});
        
</script>

<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'north',border:false"  style="height: 30px;overflow: hidden;">
          <form id="searchUserForm">
            <table>
                <tr>
                    <th>姓名:</th>
                    <td><input name="loginname" placeholder="请输入用户姓名"/><input name="test" placeholder="" style="display:none"/> <!-- 只有一个输入框时回车就会刷新整个页面，所以添加一个隐藏标签阻止刷新页面 -->
                        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fi-magnifying-glass',plain:true" onclick="searchUser();">查询</a>
                        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fi-x-circle',plain:true" onclick="cleanUser();">清空</a>
               		 </td>
                </tr>
            </table>
        </form>
    </div>
    <div data-options="region:'west',border:true,title:'组织架构'"  style="overflow: hidden;width:150px;">
    </div>
    <div data-options="region:'center',border:true,title:'用户列表'"  style="overflow: hidden;">
        <table id="userDataGrid"></table>
    </div>
</div>
<div id="userToolbar" style="display: none;">
        <a onclick="addUser();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-plus icon-green'">添加</a>
        <a onclick="deleteUser();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-x icon-red'">批量删除</a>
</div>