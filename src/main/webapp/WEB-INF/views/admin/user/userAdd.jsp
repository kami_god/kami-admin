<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<script type="text/javascript">

	$(function(){
		$('#cc').combotree({    
		    url:'${base}/sys/role/tree',    
		    multiple:true,
		    panelHeight : 'auto'
		});
		
	});

	$("#userAddForm").form({
		url:'${base}/sys/user/add',
	 	onSubmit: function(){    
	 		// do some check
	 	     progressLoad();
             var isValid = $(this).form('validate');
             if (!isValid) {
                 progressClose();
             }
             return isValid;
	    },    
	    success:function(res){ 
	    	progressClose();
	    	res = $.parseJSON(res);
	    	if(res.success){
	    		userDataGrid.datagrid('reload'); // 父页面datagrid reload
	    		$.modalDialog.handler.dialog('close');// 父页面dialog close
	    	}
	    }    

	});
/* 	$(function(){
		$("#userAddForm").form('clear');
	}); */
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 3px;">
        <form id="userAddForm" method="post">
            <table class="grid">
                <tr>
                    <td>登录名</td>
                    <td><input name="loginname" type="text" placeholder="请输入登录名称" class="easyui-validatebox" data-options="required:true" value=""></td>
                    <td>姓名</td>
                    <td><input name="username" type="text" placeholder="请输入姓名" class="easyui-validatebox" data-options="required:true" value=""></td>
                </tr>
                <tr>
                    <td>密码</td>
                    <td><input name="password" type="password" placeholder="请输入密码" class="easyui-validatebox" data-options="required:true"></td>
        <!--             <td>性别</td>
                    <td>
                        <select name="sex" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option value="0" selected="selected">男</option>
                            <option value="1" >女</option>
                        </select>
                    </td> -->
                </tr>
                                <tr>
                    <td>邮箱</td>
                    <td><input class="easyui-textbox" type="text" name="email" data-options="required:true,validType:'email'"></input></td>
                </tr>
             <!--    <tr>
                    <td>年龄</td>
                    <td><input type="text" name="age" class="easyui-numberbox"/></td>
                    <td>用户类型</td>
                    <td>
                        <select name="userType" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                            <option value="0">管理员</option>
                            <option value="1" selected="selected">用户</option>
                        </select>
                    </td>
                </tr> -->
              <!--   <tr>
                    <td>部门</td>
                    <td><select id="userAddOrganizationId" name="organizationId" style="width: 140px; height: 29px;" class="easyui-validatebox" data-options="required:true"></select></td>
                </tr> -->
                <tr>
                    <td>电话</td>
                    <td>
                        <input type="text" name="phone" class="easyui-numberbox"/>
                    </td>
                    <td>用户状态</td>
                    <td>
                        <select name="disable" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                                <option value="0">正常</option>
                                <option value="1">停用</option>
                        </select>
                    </td>
                </tr>
                <tr>
                 	<td>角色</td>
                    <td>
                       <input id="cc" name="roleIds"  >  
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
