<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<script type="text/javascript">
	var roleIds =  ${roleIds };
	$(function(){
		
		$("#userStatus").val('${user.disable}');
		$('#cc').combotree({    
		    url:'${base}/sys/role/tree',    
		    multiple:true,
		    panelHeight : 'auto',
		    value:roleIds
		});
	});

	$("#userEditForm").form({
		url:'${base}/sys/user/edit',
	 	onSubmit: function(){    
	 		// do some check
	 	     progressLoad();
             var isValid = $(this).form('validate');
             if (!isValid) {
                 progressClose();
             }
             return isValid;
	    },    
	    success:function(res){ 
	    	progressClose();
	    	res = $.parseJSON(res);
	    	if(res.success){
	    		userDataGrid.datagrid('reload'); // 父页面datagrid reload
	    		$.modalDialog.handler.dialog('close');// 父页面dialog close
	    	}
	    }    

	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 3px;">
        <form id="userEditForm" method="post">
            <table class="grid">
                <tr>
                    <td>登录名</td>
                    <td>
                    <input type="hidden" name="id" value="${user.id }">
                    <input name="loginname" type="text" placeholder="请输入登录名称" class="easyui-validatebox" data-options="required:true" value="${user.loginname }">
                    </td>
                    <td>姓名</td>
                    <td><input name="username" type="text" placeholder="请输入姓名" class="easyui-validatebox" data-options="required:true" value="${user.username }"></td>
                </tr>
                <tr>
                    <td>密码</td>
                    <td ><input name="password" type="text" placeholder="请输入密码" class="easyui-validatebox" ></td>
                 	<td colspan="2">密码为空不修改</td> 
                </tr>
                <tr>
                    <td>邮箱</td>
                    <td><input class="easyui-textbox" type="text" name="email" data-options="required:true,validType:'email'" value="${user.email }"></input></td>
                </tr>
                <tr>
                    <td>电话</td>
                    <td>
                        <input type="text" name="phone" class="easyui-numberbox" value="${user.phone }"/>
                    </td>
                    <td>用户状态</td>
                    <td>
                        <select id="userStatus" name="disable" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
                                <option value="0">正常</option>
                                <option value="1">停用</option>
                        </select>
                    </td>
                </tr>
                <tr>
                 	<td>角色</td>
                    <td>
                       <input id="cc" name="roleIds"  >  
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
