<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
    <script type="text/javascript">
    var scheduleDatagrid;
    $(function() {
     	 scheduleDatagrid = $("#scheduleDatagrid").datagrid({
    		 url:'${base}/sys/schedule/dataGrid',    
 		     columns:[[
				{
				    field : 'ck',
				    checkbox : true
				},
 		        {field:'jobId',title:'任务id',width:100,sortable:true,align:'center'},    
 		        {field:'beanName',title:'bean名称',width:200,align:'center'},    
 		        {field:'methodName',title:'方法名称',width:200,align:'center'},    
 		        {field:'params',title:'参数',width:200,align:'center'},
 		        {field:'cronExpression',title:'cron表达式',width:100,align:'center'},
 		        {field:'remark',title:'备注',width:100,align:'center'},
 		        {field:'status',title:'状态',width:50,align:'center',
 		        	formatter : function(value, row, index) {
		        	   return value == 0 ? '<font class="icon-green">正常<font/>':'<font class="icon-red">停用<font/>';
		        }}
 		       
 		    ]],
 		    fit : true,
 		    fitColumns:false,//自动宽度
 		    singleSelect:true,//单选
 	        pagination: true,//分页
             rownumbers: true,//行数
             pageSize: 20,//默认选择的分页是每页20行数据
             pageList: [10, 20, 30, 50, 100],//可以选择的分页集合
             nowrap: true,//设置为true，当数据长度超出列宽时将会自动截取
             striped: true,//设置为true将交替显示行背景。
             toolbar : '#scheduleToolbar'
    	});  
    });
    
    function doSearch(value){
    	scheduleDatagrid.datagrid('load',{beanName:value});
    }
    
    function runOnce(){
      	 var row = scheduleDatagrid.datagrid('getSelected');
    	 if(row == null){
    		 return;
    	 }
    	confirm("确定要立即执行选中的记录？",function(r){
			$.ajax({
				type: "POST",
			    url: "${base}/sys/schedule/run",
			    data: {jobId:row.jobId},
			    success: function(r){
			    	scheduleDatagrid.datagrid('reload');
			    	showMsg(r.msg);
				}
			});
    	});
    }
</script>

<div class="easyui-layout" data-options="fit:true,border:false">
    <div data-options="region:'center',border:false"  style="overflow: hidden;">
      <div id="accordion" class="easyui-accordion" fit="true">
		<div title="定时任务" data-options="iconCls:'fi-calendar'" style="overflow:auto;">
			 <table id="scheduleDatagrid"></table>
		</div>
		<div title="日志列表" data-options="iconCls:'fi-list'" style="overflow:auto;">
		</div>
	</div>
    </div>
</div>
<div id="scheduleToolbar" style="display: none;">
        <input class="easyui-searchbox" data-options="prompt:' bean名称',searcher:doSearch" style="width:200px"></input>
        <a onclick="addResourceFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-plus icon-green'">新增</a>
        <a onclick="addResourceFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-pencil icon-green'">修改</a>
        <a onclick="addResourceFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-x icon-green'">删除</a>
        <a onclick="addResourceFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-pause icon-green'">暂停</a>
        <a onclick="addResourceFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-play icon-green'">恢复</a>
        <a onclick="runOnce();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'fi-play-video icon-green'">立即执行</a>
</div>

