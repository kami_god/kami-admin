<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>登录</title>
    <%@ include file="/commons/basejs.jsp" %>
    <link rel="stylesheet" type="text/css" href="${base }/static/css/login.css">
</head>
<body  >

	<div id="app" style="margin:0 auto;width:400px;margin-top:200px;">
		<div class="easyui-panel" title="登录" style="width:400px;padding:30px 70px 20px 70px">
		<div v-if="error" style="margin-bottom:10px">
			<i style="color:red;">{{errorMsg}}</i>
		</div>
		<div style="margin-bottom:10px">
			<input class="easyui-textbox"  id="username"   style="width:100%;height:40px;padding:12px" data-options="prompt:'用户名',iconCls:'icon-man',iconWidth:38">
		</div>
		<div style="margin-bottom:10px">
			<input class="easyui-textbox" id="password" type="password" style="width:100%;height:40px;padding:12px" data-options="prompt:'password',iconCls:'icon-lock',iconWidth:38">
		</div>
		<div style="margin-bottom:10px;">
			<input class="easyui-textbox" id="captcha" type="text" style="width:58%;height:40px;padding:12px;vertical-align: middle;" data-options="prompt:'验证码'">
				<img alt="验证码！" class="pointer" :src="src" @click="refreshCode">
		</div>
		<div style="margin-bottom:10px">
			<input type="checkbox" checked="checked">
			<span>记住我</span>
		</div>
		<div>
			<a href="#" @click="login" class="easyui-linkbutton" data-options="iconCls:'icon-ok'" style="padding:5px 0px;width:100%;">
				<span style="font-size:14px;">登录</span>
			</a>
		</div>
	</div>
	
	</div>
	
		<script>
		var v = new Vue({
			el:'#app',
			data:{
				src:'captcha.jpg',
				error:false,
				errorMsg:''
			},
			beforeCreate:function(){
				//vue加载时执行
			},
			methods:{
				refreshCode:function(){
					this.src = "captcha.jpg?t=" + $.now();
				},
				login:function(){
					var username = $("#username").textbox('getValue');
					var pass = $("#password").textbox('getValue');
					var captcha = $("#captcha").textbox('getValue');
					var data = "username="+username+"&password="+pass+"&captcha="+captcha;
					if(username == ''){
						showTipMsg("用户名不能为空！");
						return;
					}
					if(pass == ''){
						showTipMsg("密码不能为空！");
						return;
					}
					
					$.ajax({
						type:"POST",
						url:"${base}/login",	
						data:data,
						dataType: "json",
						success:function(res){
							if(res.code == 1){
								window.location.href="${base}/index";
							}else{
								v.error = true;
								v.errorMsg = res.msg;
								v.refreshCode();
							}
						}
					});
				}
			}
		});
		
		// /**--点登陆或者回车按钮触发事件--**/
	  $(document).keydown(function(e) {
	    if (e.ctrlKey && e.which ==13 || e.which==13 ){
	    	v.login();
	    }
	  });
	
	</script>
</body>
</html>

