package com.kami.service;

import java.util.List;

import com.kami.entity.SysUserRole;

public interface SysUserRoleService extends BaseService<SysUserRole>{

	List<Long> findRoleIdsByUserId(Long id);

}
