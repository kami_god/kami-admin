package com.kami.service;

import java.util.Map;

import com.kami.entity.SysToken;

public interface SysTokenService extends BaseService<SysToken>{

	SysToken queryToken(String token);
	
	SysToken queryByUserId(Long id);
	
	void save(SysToken token);

	void update(SysToken token);

	SysToken createToken(Long id);

}
