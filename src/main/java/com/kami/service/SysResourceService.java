package com.kami.service;

import java.util.List;

import com.kami.entity.SysResource;
import com.kami.entity.SysUser;
import com.kami.model.Tree;

public interface SysResourceService extends BaseService<SysResource>{

	List<Tree> selectTree(SysUser user);

	List<Tree> selectAllTree();

	List<SysResource> selectAll();

	void insert(SysResource resource);

	List<Tree> selectAllMenu();

	SysResource findById(Integer id);

	void updateById(SysResource resource);

	void deleteById(Integer id);


}
