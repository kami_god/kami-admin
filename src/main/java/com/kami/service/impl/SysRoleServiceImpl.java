package com.kami.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kami.dao.SysRoleMapper;
import com.kami.dao.SysRoleResourceMapper;
import com.kami.entity.SysRole;
import com.kami.entity.SysRoleExample;
import com.kami.entity.SysRoleExample.Criteria;
import com.kami.entity.SysRoleResource;
import com.kami.entity.SysRoleResourceExample;
import com.kami.model.Tree;
import com.kami.service.SysRoleService;
import com.kami.utils.ShiroUtils;

@Service
public class SysRoleServiceImpl implements SysRoleService{

	@Autowired
	private SysRoleMapper sysRoleMapper;
	@Autowired
	private SysRoleResourceMapper sysRoleResourceMapper;
	
	
	public List<SysRole> findByRole(SysRole role) {
		SysRoleExample example = new SysRoleExample();
		Criteria criteria = example.createCriteria();
		List<SysRole> list = sysRoleMapper.selectByExample(example);
		return list;
	}


	public void add(SysRole role) {
		role.setCreateTime(new Date());
		Long createUserId = Long.valueOf(ShiroUtils.getShiroUser().getId());
		role.setCreateUserId(createUserId);
		sysRoleMapper.insertSelective(role);
	}


	public void delete(String id) {
		sysRoleMapper.deleteByPrimaryKey(Long.valueOf(id));
	}


	public SysRole findById(Long id) {
		SysRole role = sysRoleMapper.selectByPrimaryKey(id);
		SysRoleResourceExample example = new SysRoleResourceExample();
		com.kami.entity.SysRoleResourceExample.Criteria criteria = example.createCriteria();
		criteria.andRoleIdEqualTo(id);
		List<SysRoleResource> list = sysRoleResourceMapper.selectByExample(example);
		List<Long> menuIdList = new ArrayList<>();
		for (SysRoleResource roleResource : list) {
			menuIdList.add(roleResource.getResourceId());
		}
		role.setMenuIdList(menuIdList);
		return role;
	}


	public void update(SysRole role) {
		sysRoleMapper.updateByPrimaryKeySelective(role);
	}


	public List<SysRole> findList() {
		return sysRoleMapper.selectByExample(null);
	}


	public List<Tree> selectTree() {
		List<SysRole> lists = sysRoleMapper.selectByExample(null);
		List<Tree> trees = new ArrayList<>();
		for (SysRole sysRole : lists) {
			Tree tree = new Tree();
			tree.setId(sysRole.getRoleId());
			tree.setText(sysRole.getRoleName());
			trees.add(tree);
		}
		return trees;
	}




	
	
	

}
