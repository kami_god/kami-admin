package com.kami.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kami.dao.SysRoleMapper;
import com.kami.dao.SysRoleResourceMapper;
import com.kami.entity.SysRole;
import com.kami.entity.SysRoleResource;
import com.kami.entity.SysRoleResourceExample;
import com.kami.entity.SysRoleResourceExample.Criteria;
import com.kami.service.SysRoleResourceService;

@Service
public class SysRoleResourceServiceImpl implements SysRoleResourceService{

	@Autowired
	private SysRoleResourceMapper sysRoleResourceMapper;
	
	@Transactional
	public void update(SysRole role) {
		// 先全部删除，后添加
		SysRoleResourceExample example = new SysRoleResourceExample();
		Criteria criteria = example.createCriteria();
		criteria.andRoleIdEqualTo(role.getRoleId());
		sysRoleResourceMapper.deleteByExample(example);
		
		// 添加
		List<Long> menuIdList = role.getMenuIdList();
		for (Long id : menuIdList) {
			SysRoleResource roleResource = new SysRoleResource();
			roleResource.setResourceId(id);
			roleResource.setRoleId(role.getRoleId());
			sysRoleResourceMapper.insertSelective(roleResource);
		}
	}

}
