package com.kami.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kami.dao.SysUserRoleMapper;
import com.kami.entity.SysUserRole;
import com.kami.entity.SysUserRoleExample;
import com.kami.entity.SysUserRoleExample.Criteria;
import com.kami.service.SysUserRoleService;

@Service
public class SysUserRoleServiceImpl implements SysUserRoleService{

	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;

	public List<Long> findRoleIdsByUserId(Long userId) {
		SysUserRoleExample example = new SysUserRoleExample();
		Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		List<SysUserRole> list = sysUserRoleMapper.selectByExample(example );
		List<Long> ids = new ArrayList<>();
		for (SysUserRole userRole : list) {
			ids.add(userRole.getRoleId());
		}
		return ids;
	}
	
}
