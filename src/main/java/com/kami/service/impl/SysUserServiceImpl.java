package com.kami.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kami.dao.SysUserMapper;
import com.kami.dao.SysUserRoleMapper;
import com.kami.entity.SysUser;
import com.kami.entity.SysUserExample;
import com.kami.entity.SysUserExample.Criteria;
import com.kami.entity.SysUserRole;
import com.kami.entity.SysUserRoleExample;
import com.kami.model.UserVo;
import com.kami.service.SysUserService;
import com.kami.utils.ShiroUtils;

@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService{

	@Autowired
	private SysUserMapper sysUserMapper;
	@Autowired 
	private SysUserRoleMapper sysUserRoleMapper;
	
	public List<SysUser> findByCondition(UserVo user) {
		SysUserExample example = new SysUserExample();
		Criteria criteria = example.createCriteria();
		if (StringUtils.isNotBlank(user.getLoginname())) {
//			criteria.andLoginnameLike("%"+user.getLoginname()+"%");
			criteria.andUsernameLike("%"+user.getLoginname()+"%");
		}
		return sysUserMapper.selectByExample(example);
	}

	public void save(SysUser user) {
		sysUserMapper.insertSelective(user);
		SysUser temp = sysUserMapper.selectByLoginName(user.getLoginname());
		//  save role
		List<Long> roleIds = user.getRoleIds();
		for (Long roleId : roleIds) {
			SysUserRole record = new SysUserRole();
			record.setRoleId(roleId);
			record.setUserId(temp.getId());
			sysUserRoleMapper.insertSelective(record);
		}
		
	}

	public void delete(String id) {
		sysUserMapper.deleteByPrimaryKey(Long.valueOf(id));
	}

	public void deleteBatch(String[] ids) {
		SysUserExample example = new SysUserExample();
		Criteria criteria = example.createCriteria();
		List<Long> idList = new ArrayList<>();
		for (String id : ids) {
			idList.add(Long.valueOf(id));
		}
		criteria.andIdIn(idList);
		sysUserMapper.deleteByExample(example);
	}

	public SysUser findById(Long id) {
		return sysUserMapper.selectByPrimaryKey(id);
	}

	public void updateUser(SysUser user) {
		if (StringUtils.isEmpty(user.getPassword())) {
			user.setPassword(null);
		}
		sysUserMapper.updateByPrimaryKeySelective(user);
		Long userId = user.getId();
		//  修改权限
		SysUserRoleExample example = new SysUserRoleExample();
		com.kami.entity.SysUserRoleExample.Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(userId);
		sysUserRoleMapper.deleteByExample(example);
		List<Long> roleIds = user.getRoleIds();
		for (Long roleId : roleIds) {
			SysUserRole record = new SysUserRole();
			record.setRoleId(roleId);
			record.setUserId(userId);
			sysUserRoleMapper.insertSelective(record );
		}
		
	}

	public Set<String> findRoles() {
		SysUser user = ShiroUtils.getShiroUser();
		if (null == user) {
			return Collections.emptySet();
		}
		
		return null;
	}

	public List<String> findAllPerms(Long userId) {
		List<String> permsList = sysUserRoleMapper.findAllPerms(userId);
		return permsList;
	}

	@Override
	public SysUser login(String loginName, String password) {
		SysUserExample example = new SysUserExample();
		example.createCriteria().andLoginnameEqualTo(loginName).andPasswordEqualTo(password);
		List<SysUser> list = sysUserMapper.selectByExample(example);
		return list.size() > 0 ? list.get(0) : null;
	}

	@Override
	public SysUser findOne(long id) {
		return sysUserMapper.selectByPrimaryKey(id);
	}


	
}
