package com.kami.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kami.dao.ScheduleJobLogMapper;
import com.kami.entity.ScheduleJobLog;
import com.kami.service.ScheduleJobLogService;

@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl implements ScheduleJobLogService{

	@Autowired
	private ScheduleJobLogMapper scheduleJobLogMapper;
	
	@Override
	public void save(ScheduleJobLog log) {
		scheduleJobLogMapper.insertSelective(log);
	}

	
}
