package com.kami.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kami.dao.SysResourceMapper;
import com.kami.entity.SysResource;
import com.kami.entity.SysResourceExample;
import com.kami.entity.SysResourceExample.Criteria;
import com.kami.entity.SysUser;
import com.kami.model.Tree;
import com.kami.service.SysResourceService;

@Service
public class SysResourceServiceImpl implements SysResourceService{
	
	private static final int RESOURCE_MENU = 0; // 菜单
	
	@Autowired
	private SysResourceMapper sysResourceMapper;

	public List<SysResource> selectAll() {
		return sysResourceMapper.selectByExample(null);
	}

	public void insert(SysResource resource) {
		sysResourceMapper.insertSelective(resource);
	}

	public List<Tree> selectAllMenu() {
		SysResourceExample example = new SysResourceExample();
		Criteria criteria = example.createCriteria();
		criteria.andResourceTypeEqualTo(RESOURCE_MENU);
		List<SysResource> resources = sysResourceMapper.selectByExample(example);
		List<Tree> trees = new ArrayList<Tree>();
        if (resources == null) {
            return trees;
        }
        for (SysResource resource : resources) {
            Tree tree = new Tree();
            tree.setId(Long.valueOf(resource.getId()));
            tree.setpId(resource.getPid());
            tree.setText(resource.getName());
            tree.setIconCls(resource.getIcon());
            tree.setAttributes(resource.getUrl());
            tree.setState(resource.getOpened());
            trees.add(tree);
        }
        return trees;
	}

	public List<Tree> selectTree(SysUser user) {
		Long id = user.getId();
		List<Tree> trees = new ArrayList<Tree>();
		if (id == 1) {
			// 超级管理员
			SysResourceExample example = new SysResourceExample();
			Criteria criteria = example.createCriteria();
			criteria.andResourceTypeEqualTo(RESOURCE_MENU);
			List<SysResource> resources = sysResourceMapper.selectByExample(example);
	        if (resources == null) {
	            return trees;
	        }
	        for (SysResource resource : resources) {
	            Tree tree = new Tree();
	            tree.setId(Long.valueOf(resource.getId()));
	            tree.setpId(resource.getPid());
	            tree.setText(resource.getName());
	            tree.setIconCls(resource.getIcon());
	            tree.setAttributes(resource.getUrl());
	            tree.setState(resource.getOpened());
	            tree.setOpenMode(resource.getOpenMode());
	            trees.add(tree);
	        }
	        return trees;
		}
		
		List<SysResource> resList = sysResourceMapper.findResourceByUserId(id.intValue());
		if (resList == null) {
			return trees;
		}
        for (SysResource resource : resList) {
        	if (resource.getResourceType() != RESOURCE_MENU) {
				continue;
			}
            Tree tree = new Tree();
            tree.setId(Long.valueOf(resource.getId()));
            tree.setpId(resource.getPid());
            tree.setText(resource.getName());
            tree.setIconCls(resource.getIcon());
            tree.setAttributes(resource.getUrl());
            tree.setState(resource.getOpened());
            tree.setOpenMode(resource.getOpenMode());
            trees.add(tree);
        }
		return trees;

	}

	public SysResource findById(Integer id) {
		return sysResourceMapper.selectByPrimaryKey(id);
	}

	public void updateById(SysResource resource) {
		sysResourceMapper.updateByPrimaryKey(resource);
	}

	public void deleteById(Integer id) {
		sysResourceMapper.deleteByPrimaryKey(id);
	}

	public List<Tree> selectAllTree() {
		List<SysResource> resources = sysResourceMapper.selectByExample(null);
		List<Tree> trees = new ArrayList<Tree>();
        if (resources == null) {
            return trees;
        }
        for (SysResource resource : resources) {
        	 Tree tree = new Tree();
        	 tree.setId(Long.valueOf(resource.getId()));
             tree.setpId(resource.getPid());
             tree.setText(resource.getName());
             tree.setIconCls(resource.getIcon());
             tree.setAttributes(resource.getUrl());
             tree.setState(resource.getOpened());
             tree.setOpenMode(resource.getOpenMode());
             trees.add(tree);
        }
        return trees;
	}

	public List<SysResource> findAll() {
		return sysResourceMapper.selectByExample(null);
	}
	
	
}
