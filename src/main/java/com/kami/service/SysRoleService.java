package com.kami.service;

import java.util.List;

import com.kami.entity.SysRole;
import com.kami.model.Tree;

public interface SysRoleService extends BaseService<SysRole> {

	SysRole findById(Long id);

	List<SysRole> findByRole(SysRole role);

	void add(SysRole role);

	void delete(String id);

	void update(SysRole role);

	List<Tree> selectTree();


}
