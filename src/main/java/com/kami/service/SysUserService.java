package com.kami.service;

import java.util.List;

import com.kami.entity.SysUser;
import com.kami.model.UserVo;

public interface SysUserService extends BaseService<SysUser>{

	SysUser findById(Long id);

	List<SysUser> findByCondition(UserVo user);

	void save(SysUser user);

	void delete(String id);

	void deleteBatch(String[] ids);

	void updateUser(SysUser user);

	List<String> findAllPerms(Long userId);

	SysUser login(String loginName, String password);

	SysUser findOne(long id);

}
