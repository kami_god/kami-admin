package com.kami.service;

import com.kami.entity.ScheduleJobLog;

public interface ScheduleJobLogService extends BaseService<ScheduleJobLog>{

	void save(ScheduleJobLog log);

}
