package com.kami.service;

import com.kami.entity.SysRole;
import com.kami.entity.SysRoleResource;

public interface SysRoleResourceService extends BaseService<SysRoleResource>{

	void update(SysRole role);

}
