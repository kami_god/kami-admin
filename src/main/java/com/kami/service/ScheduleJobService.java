package com.kami.service;

import java.util.List;

import com.kami.entity.ScheduleJob;

public interface ScheduleJobService extends BaseService<ScheduleJob>{

	List<ScheduleJob> findByJob(ScheduleJob job);

	void run(Long jobId);

}
