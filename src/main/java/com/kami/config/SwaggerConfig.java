package com.kami.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * swagger2 的配置
 * @author dong
 *
 */
@EnableSwagger2
public class SwaggerConfig {
	
		@Bean
	    public Docket api() {
	        return new Docket(DocumentationType.SWAGGER_2)
	                .select()
	                .apis(RequestHandlerSelectors.basePackage("com.kami.api"))
	                .build()
	                .globalOperationParameters(setHeaderToken())
	                .apiInfo(apiInfo());
	    }
	    private ApiInfo apiInfo() {
	        return new ApiInfoBuilder()
	                .title("kami-admin项目接口文档")
	                .description("kami-admin项目接口测试")
	                .version("1.0.0")
	                .termsOfServiceUrl("")
	                .license("")
	                .licenseUrl("")
	                .build();
	    }
	    private List<Parameter> setHeaderToken() {
	        ParameterBuilder tokenPar = new ParameterBuilder();
	        List<Parameter> pars = new ArrayList<>();
	        tokenPar.name("token").description("token").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
	        pars.add(tokenPar.build());
	        return pars;
	    }

}
