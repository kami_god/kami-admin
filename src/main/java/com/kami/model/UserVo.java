package com.kami.model;

public class UserVo {
	
	private String loginname;

	public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	@Override
	public String toString() {
		return "UserVo [loginname=" + loginname + "]";
	}
	
	
	


}