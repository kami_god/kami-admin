package com.kami.model;

import java.util.List;

public class DataGrid {
	
    private long total; // 总记录 
    private List rows; //显示的记录  
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List getRows() {
		return rows;
	}
	public void setRows(List rows) {
		this.rows = rows;
	}
	public DataGrid(long total, List rows) {
		super();
		this.total = total;
		this.rows = rows;
	}
	public DataGrid() {
		super();
		// TODO Auto-generated constructor stub
	}
    
    

}
