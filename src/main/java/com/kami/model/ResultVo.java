package com.kami.model;

/**
 * rest接口返回数据对象
 */
public class ResultVo {
	/**
	 * 结果0-失败，1-成功
	 */
	private int code;
	/**
	 * 结果代码
	 */
	private boolean success;
	/**
	 * 结果消息
	 */
	private String msg;
	/**
	 * 结果数据
	 */
	private Object data;
	
	public static final boolean SUCCESS = true;
	public static final boolean FAILURE = false;
	
	public static ResultVo error(){
		return new ResultVo(0, FAILURE);
	}
	public static ResultVo error(String msg){
		return new ResultVo(0, FAILURE,msg);
	}
	public static ResultVo success(){
		return new ResultVo(1, SUCCESS);
	}
	public static ResultVo success(String msg){
		return new ResultVo(1, SUCCESS,msg);
	}
	public static ResultVo success(String msg,Object data){
		return new ResultVo(1, SUCCESS, msg, data);
	}
	
	public ResultVo(int code, boolean success, String msg) {
		super();
		this.code = code;
		this.success = success;
		this.msg = msg;
	}
	public ResultVo(int code, boolean success) {
		super();
		this.code = code;
		this.success = success;
	}
	public ResultVo(int code, boolean success, String msg, Object data) {
		super();
		this.code = code;
		this.success = success;
		this.msg = msg;
		this.data = data;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	
	
}
