package com.kami.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("testTask")
public class TestTask {
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	public void test(String param){
		logger.info("我是带参的{} ---",param);
	}

	
	public void test2(){
		logger.info("我是无参的");
	}
}
