package com.kami.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kami.model.ResultVo;

import springfox.documentation.annotations.ApiIgnore;

@RequestMapping("/api1/test")
@RestController
public class Test1Controller {
	
	@ApiIgnore
	@GetMapping("/hello1")
	public ResultVo hello1(String msg){
		System.out.println("Hello : "+ msg);
		return ResultVo.success("hello");
	}

}
