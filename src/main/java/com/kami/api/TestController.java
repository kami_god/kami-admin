package com.kami.api;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.kami.annotation.IgnoreAuth;
import com.kami.annotation.LoginUser;
import com.kami.entity.SysToken;
import com.kami.entity.SysUser;
import com.kami.model.ResultVo;
import com.kami.realm.PasswordHash;
import com.kami.service.SysTokenService;
import com.kami.service.SysUserService;
import com.kami.validate.Assert;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@Api(value="测试controller",tags={"测试接口"})
@RequestMapping("/api/test")
@RestController
public class TestController {
	
	@Autowired
	private SysTokenService tokenService;
	
	@Autowired
	private SysUserService userService;
	
	@Autowired
	private PasswordHash passwordHash;
	
	@ApiOperation(value="测试hello")
	@ApiImplicitParams({
		@ApiImplicitParam(name="msg",value="打印消息",dataType="string", paramType = "query",example="hello world",required=true)
	})
	@GetMapping("/hello")   //@RequestParam(value = "msg") 
	public ResultVo hello( String msg){
		System.out.println("Hello : "+ msg);
		return ResultVo.success("hello");
	}
	
	@IgnoreAuth
	@ApiOperation(value="登录获取令牌token")
	@PostMapping("/login")
	public ResultVo login(String loginName,String password){
		Assert.isBlank(loginName, "登录名不能为空!");
		Assert.isBlank(password, "密码不能为空!");
		SysUser user = userService.login(loginName,passwordHash.md5(password, loginName));//登录名做盐
		Assert.isNull(user, "用户名或密码错误");
		SysToken token = tokenService.createToken(user.getId());
		return new ResultVo(1, ResultVo.SUCCESS, "登录成功", token);
	}
	
	/**
	 * 测试注入用户
	 * @return
	 */
	@ApiOperation(value="注入用户")
	@PostMapping("/inject")
	public ResultVo testInject(@LoginUser SysUser user){
		System.out.println(user.getUsername());
		return ResultVo.success("注入成功", user);
	}
	
	@ApiIgnore
	@GetMapping("/hello1")
	public ResultVo hello1(String msg){
		System.out.println("Hello : "+ msg);
		return ResultVo.success("hello");
	}
}
