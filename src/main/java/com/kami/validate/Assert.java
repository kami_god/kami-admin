package com.kami.validate;

import org.apache.commons.lang3.StringUtils;

import com.kami.exception.IException;

public class Assert {
	
	public static void isBlank(String str,String messsage){
		if (StringUtils.isBlank(str)) {
			throw new IException(messsage);
		}
	}

	
	public static void isNull(Object obj,String message){
		if (null == obj) {
			throw new IException(message);
		}
	}
}
