package com.kami.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kami.controller.LoginAuthController;
import com.kami.model.ResultVo;


@ControllerAdvice
public class GlobalExceptionHandler {

	private final static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ResponseBody
    @ExceptionHandler(IException.class)//
    public ResultVo allExceptionHandler(Exception exception){
		ResultVo vo = new ResultVo(-1, ResultVo.FAILURE, "失败", exception.getMessage());
		logger.error("出错了", exception);
        return vo;
    }
	
}
