package com.kami.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CommonsController extends BaseController{

    /**
     * 图标页
     */
    @GetMapping("icons.html")
    public String icons() {
    	logger.info(" icons ");
        return "icons";
    }
}
