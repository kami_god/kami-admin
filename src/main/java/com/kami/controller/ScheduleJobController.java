package com.kami.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kami.entity.ScheduleJob;
import com.kami.model.DataGrid;
import com.kami.model.ResultVo;
import com.kami.service.ScheduleJobService;

/**
 * 任务调度控制器
 * @author dong
 *
 */
@RequestMapping("sys/schedule")
@Controller
public class ScheduleJobController extends BaseController{
	
	
	@RequestMapping("/manager")
	public String manager(){
		return "admin/schedule/schedule";
	}
	
	@Autowired
	private ScheduleJobService scheduleJobService;
	
	/**
	 * 数据网格列表
	 * @param job
	 * @param page
	 * @param rows
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/dataGrid")
	public Object datagrid(ScheduleJob job,Integer page,Integer rows){
		logger.info("schedule dataGrid");
		PageHelper.startPage(page, rows);
		List<ScheduleJob> jobs = scheduleJobService.findByJob(job);
		PageInfo<ScheduleJob> pageInfo = new PageInfo<>(jobs);
		return new DataGrid(pageInfo.getTotal(),pageInfo.getList());
	}

	@ResponseBody
	@RequestMapping("/run")
	public ResultVo run(Long jobId){
		logger.info("run:"+jobId);
		scheduleJobService.run(jobId);
		return ResultVo.success("成功");
	}
}
