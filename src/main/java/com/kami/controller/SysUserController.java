package com.kami.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.credential.Md5CredentialsMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kami.entity.SysRole;
import com.kami.entity.SysUser;
import com.kami.entity.SysUserExample;
import com.kami.entity.SysUserExample.Criteria;
import com.kami.model.DataGrid;
import com.kami.model.ResultVo;
import com.kami.model.UserVo;
import com.kami.realm.PasswordHash;
import com.kami.service.SysRoleService;
import com.kami.service.SysUserRoleService;
import com.kami.service.SysUserService;

import ch.qos.logback.core.pattern.SpacePadder;

/**
 * 系统用户控制器
 * @author dong
 *
 */
@RequestMapping("sys/user")
@Controller
public class SysUserController extends BaseController{

	
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	@Autowired
	private PasswordHash passwordHash;
	
	@GetMapping("/manager")
	public String manager(){
		return "admin/user/user";
	}
	
	@GetMapping("addPage")
	public String addPage(){
		return "admin/user/userAdd";
	}
	
	@GetMapping("editUserPage")
	public String editPage(String id,Model model){
		logger.info("id: "+id);
		SysUser user = sysUserService.findById(Long.valueOf(id));
		List<Long> roleIds = sysUserRoleService.findRoleIdsByUserId(Long.valueOf(id));
		model.addAttribute("user", user);
		model.addAttribute("roleIds", roleIds);
		return "admin/user/userEdit";
	}
	
	/**
	 * 用户列表
	 * @param user
	 * @param page
	 * @param rows
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@PostMapping("/dataGrid")
	@ResponseBody
	public Object dataGrid(UserVo user,Integer page,Integer rows){
		System.out.println(user);
		PageHelper.startPage(page, rows);
		List<SysUser> list = sysUserService.findByCondition(user);
		PageInfo pageInfo = new PageInfo<>(list);
		return new DataGrid(pageInfo.getTotal(),pageInfo.getList());	
	}
	
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	@ResponseBody
	@PostMapping("/add")
	public ResultVo add(@Valid SysUser user){
		System.out.println("add : "+ user);
		user.setCreateDate(new Date());
		user.setPassword(passwordHash.md5(user.getPassword(), user.getLoginname()));
		sysUserService.save(user);
		return ResultVo.success("添加成功");
	}
	
	/**
	 * 批量删除用户
	 * @param ids
	 * @return
	 */
	@ResponseBody
	@PostMapping("/delete")
	public ResultVo delete(@RequestParam(value="ids[]") String[] ids){
		if (ids.length == 1) {
			sysUserService.delete(ids[0]);
		}else{
			sysUserService.deleteBatch(ids);
		}
		return ResultVo.success("删除成功！");
	}
	
	/**
	 * 编辑用户
	 * @param user
	 * @return
	 */
	@ResponseBody
	@PostMapping("/edit")
	public ResultVo edit(SysUser user){
		String password = user.getPassword();
		if (StringUtils.isNoneBlank(password)) {
			user.setPassword(passwordHash.md5(user.getPassword(), user.getLoginname()));
		}
		sysUserService.updateUser(user);
		return ResultVo.success("成功");
	}
}
