package com.kami.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.code.kaptcha.Producer;
import com.kami.model.Constants;
import com.kami.model.ResultVo;
import com.kami.utils.ShiroUtils;



/**
 * 登录验证控制器
 * @author dong
 *
 */
@Controller
public class LoginAuthController extends BaseController{

	private final static Logger logger = LoggerFactory.getLogger(LoginAuthController.class);
	
	@Autowired
	private Producer producer;
	
	@RequestMapping("captcha.jpg")
	public void captcha(HttpServletResponse response)throws ServletException, IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtils.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}
	
	/**
	 * 登录
	 * @param username
	 * @param password
	 * @param captcha //验证码
	 * @return
	 */
	@ResponseBody
	@PostMapping("/login")
	public ResultVo postLogin(
			@RequestParam String username,
			@RequestParam String password,
			@RequestParam String captcha
			){
		logger.info(username+"-"+password);
		String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
		if (!captcha.equalsIgnoreCase(kaptcha)) {
			return ResultVo.error("验证码不正确");
		}
		
		try {
			Subject subject = ShiroUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(username, password);
			token.setRememberMe(true);
			subject.login(token);
		}catch (UnknownAccountException e) {
			return ResultVo.error("用户名不存在");
		} catch (IncorrectCredentialsException e) {
			return ResultVo.error("密码不正确");
		}
		
		return ResultVo.success();
	}
	
	/**
	 * get请求的login
	 * @return
	 */
	@GetMapping("/login")
	public String login(){
		logger.info("GET请求登录");
		if (SecurityUtils.getSubject().isAuthenticated()) {
			return "redirect:/index";
		}
		return "login";
	}
	
	/**
	 * 跳到首页
	 * @return
	 */
	@GetMapping("/")
	public String index(){
		System.out.println("redirect:/index");
		return "redirect:/index";
	}
	
	/**
	 * 跳到首页
	 * @param model
	 * @return
	 */
	@GetMapping("/index")
	public String index(Model model,HttpSession session){
		System.out.println(" SessionId --> "+ session.getId());
		return "index";
	}
	
 
	
}
