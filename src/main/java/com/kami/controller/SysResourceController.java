package com.kami.controller;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kami.entity.SysResource;
import com.kami.entity.SysUser;
import com.kami.model.ResultVo;
import com.kami.model.Tree;
import com.kami.service.SysResourceService;
import com.kami.utils.ShiroUtils;


/**
 * 资源管理控制器
 * @author dong
 *
 */
@RequestMapping("sys/resource")
@Controller
public class SysResourceController extends BaseController{
	
	@Autowired
	private SysResourceService sysResourceService;
	
    /**
     * 菜单树
     *
     * @return
     */
    @PostMapping("/tree")
    @ResponseBody
    public List<Tree> tree(){
    	SysUser user = ShiroUtils.getShiroUser();
    	return sysResourceService.selectTree(user);
    }
    
    /**
     * 查询所有的tree
     * @return
     */
    @PostMapping("/allTree")
    @ResponseBody
    public List<Tree> allTree(){
    	logger.info(" allTree ");
    	return sysResourceService.selectAllTree();
    }
	
	@GetMapping("/manager")
	public String manager(){
		return "admin/resource/resource";
	}
	
	
	/**
	 * 资源树形网格
	 * @return
	 */
	@PostMapping("/treeGrid")
	@ResponseBody
	public List<SysResource> treeGrid(){
		return sysResourceService.selectAll();
	}
	
    /**
     * 添加资源页
     *
     * @return
     */
    @GetMapping("/addPage")
    public String addPage() {
        return "admin/resource/resourceAdd";
    }
	
    /**
     * 添加资源
     * @param resource
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public ResultVo add(@Valid SysResource resource){
    	resource.setCreateTime(new Date());
        Integer type = resource.getResourceType();
        if (null != type && type == 0) {
            resource.setOpenMode(null);
        }
        sysResourceService.insert(resource);
    	return ResultVo.success();
    }
    
	/**
	 * 资源树形网格下拉
	 * @return
	 */
	@PostMapping("/allMenu")
	@ResponseBody
	public List<Tree> allMenu(){
		return sysResourceService.selectAllMenu();
	}
	
	  /**
     * 编辑资源页
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/editPage")
    public String editPage(Model model, Integer id) {
    	SysResource resource = sysResourceService.findById(id);
    	model.addAttribute("resource", resource);
        return "admin/resource/resourceEdit";
    }
    
    /**
     * 编辑资源
     * @param resource
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public ResultVo edit(@Valid SysResource resource){
    	sysResourceService.updateById(resource);
    	return ResultVo.success("编辑成功");
    }

    /**
     * 删除资源
     * @param id
     * @return
     */
    @RequiresPermissions("sys:resource:delete")
    @RequestMapping("/delete")
    @ResponseBody
    public ResultVo delete(Integer id){
    	sysResourceService.deleteById(id);
    	return ResultVo.success("删除成功");
    }
}
