package com.kami.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kami.entity.SysRole;
import com.kami.model.DataGrid;
import com.kami.model.ResultVo;
import com.kami.model.Tree;
import com.kami.service.SysRoleResourceService;
import com.kami.service.SysRoleService;


/**
 * 系统角色控制器
 * @author dong
 *
 */
@RequestMapping("sys/role")
@Controller
public class SysRoleController extends BaseController{
	
	@Autowired
	private SysRoleService sysRoleService;
	
	@Autowired
	private SysRoleResourceService sysRoleResourceService;
	
	@ModelAttribute
	public void getRole(@RequestParam(value="roleId",required=false) Long id,Model model){
		if (id != null) {
			SysRole role = sysRoleService.findById(id);
			model.addAttribute("sysRole", role);
		}
	}
	
	@GetMapping("/manager")
	public String manager(){
		return "admin/role/role";
	}
	
	@GetMapping("/addRolePage")
	public String addRolePage(){
		return "admin/role/roleAdd";
	}

	@GetMapping("/editRolePage")
	public String editRolePage(Long id,Model model){
		SysRole role = sysRoleService.findById(id);
		model.addAttribute("role", role);
		return "admin/role/roleEdit";
	}
	
	/**
	 * 授权页面
	 * @param id
	 * @param model
	 * @return
	 */
	@GetMapping("/grantPage")
	public String grantPage(Long id,Model model){
		model.addAttribute("id", id);
		return "admin/role/roleGrant";
	}
	
	
	/**
	 * 角色列表
	 * @param role
	 * @param page
	 * @param rows
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@ResponseBody
	@PostMapping("/dataGrid")
	public Object dataGrid(SysRole role,Integer page,Integer rows){
		logger.info(" dataGrid ");
		PageHelper.startPage(page, rows);
		List<SysRole> roles = sysRoleService.findByRole(role);
		PageInfo pageInfo = new PageInfo<>(roles);
		return new DataGrid(pageInfo.getTotal(),pageInfo.getList());
	}
	
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/add")
	public ResultVo add(SysRole role){
		sysRoleService.add(role);
		return ResultVo.success("添加成功！");
	}
	
	/**
	 * 删除角色
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/delete")
	public ResultVo delete(String id){
		sysRoleService.delete(id);
		return ResultVo.success("删除成功！");
	}
	
	/**
	 * 更新角色
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/update")
	public ResultVo update(SysRole role){
		sysRoleService.update(role);
		return ResultVo.success("更新成功！");
	}
	

	/**
	 * 授权
	 * @param role
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/grant")
	public ResultVo grant(SysRole role){
		sysRoleResourceService.update(role);
		return ResultVo.success("授权成功！");
	}
	
	
	/**
	 * 获取角色的信息
	 * @param id
	 * @return
	 */
	@ResponseBody
	@PostMapping("/getMenuIdListByRoleId")
	public SysRole getMenuIdListByRoleId(Long id){
		logger.info(" id= "+id);
		return sysRoleService.findById(id);
	}
	
	/**
	 * 获取所有角色列表
	 * @return
	 */
	@ResponseBody
	@PostMapping("/tree")
	public List<Tree> tree(){
		return sysRoleService.selectTree();
	}
	
	
}
