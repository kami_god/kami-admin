/*
SQLyog Ultimate v8.32 
MySQL - 5.7.10-log : Database - kami-admin
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kami-admin` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `kami-admin`;

/*Table structure for table `sys_resource` */

DROP TABLE IF EXISTS `sys_resource`;

CREATE TABLE `sys_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `perms` varchar(500) DEFAULT NULL,
  `open_mode` varchar(10) DEFAULT NULL,
  `description` varchar(10) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `seq` int(11) DEFAULT '0',
  `status` int(11) DEFAULT '0',
  `opened` int(11) DEFAULT '1',
  `resource_type` int(11) DEFAULT '0' COMMENT '资源类型',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `sys_resource` */

insert  into `sys_resource`(`id`,`name`,`url`,`perms`,`open_mode`,`description`,`icon`,`pid`,`seq`,`status`,`opened`,`resource_type`,`create_time`) values (1,'权限管理',NULL,NULL,NULL,'权限管理','fi-folder',NULL,0,0,1,0,'2017-09-19 00:00:01'),(2,'资源管理','sys/resource/manager',NULL,'iframe','资源管理','fi-database',1,0,0,1,0,'2017-09-19 00:00:01'),(8,'列表','sys/resource/treeGrid',NULL,'ajax',NULL,'fi-list',2,1,0,1,1,'2017-09-19 17:00:41'),(11,'用户管理','sys/user/manager','','ajax',NULL,'fi-database',1,0,0,1,0,'2017-09-26 16:22:09');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `create_user_id` bigint(20) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

/*Table structure for table `sys_role_resource` */

DROP TABLE IF EXISTS `sys_role_resource`;

CREATE TABLE `sys_role_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL,
  `resource_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_resource` */

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `loginname` varchar(255) NOT NULL,
  `email` varchar(40) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `org_id` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `disable` varchar(2) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginname` (`loginname`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`username`,`loginname`,`email`,`phone`,`org_id`,`password`,`disable`,`photo`,`create_date`) values (1,'jwz','admin','467812367@qq.com','1359693235',NULL,'a66abb5684c45962d887564f08346e8d','0',NULL,'2017-09-10 14:16:11');

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user_role` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
